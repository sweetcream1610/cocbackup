﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="yamada")]
public class DataSheet:ScriptableObject
{
 //   public List<Datasheet> enemyData = new List<Datasheet>();
    [SerializeField] string dataName;
    [SerializeField] int maxHP;
    [SerializeField] int hp;
    [SerializeField] int atk;
    [SerializeField] int def;
    [SerializeField] int itemData;
    [SerializeField] string itemName;
    [SerializeField] GameObject thisObject;
    string itemDataName;
    public GameObject ID
    {
        get
        {
            return thisObject;
        }
        set
        {
            thisObject = value; 
        }
        
    }
    public string Name
    {
        get
        {
            return dataName;
        }
    }
    public int MaxHP
    {
        get
        {
            return maxHP;
        }
    }
    public int HP
    {
        get
        {
            return hp;
        }
    }
    public int ATK
    {
        get
        {
            return atk;
        }
    }
    public int DEF
    {
        get
        {
            return def;
        }
    }
    public int ItemData
    {
        get
        {
            return itemData;
        }
    }
    public string ItemName
    {
        get
        {
            return itemName;
        }
    }
}


