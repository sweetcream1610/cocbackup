﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActionManager : MonoBehaviour
{
    [SerializeField]
   private GameObject Attack; //敵に衝突したら敵オブジェクトをデストロイ
    [SerializeField]
   private GameObject Menu; //メニュー画面を開く
    private void Start()
    {
       
    }
    void Update()
    {
        Fire();
    }
    /// <summary>
    /// 攻撃シミュレート関数
    /// </summary>
    private void Fire() 
    {
        if (Input.GetMouseButtonDown(1))
        {
            Attack.SetActive(true);
          
        }
        if (Input.GetMouseButtonUp(1))
        {
            Attack.SetActive(false);
        }
            
    }
  }
