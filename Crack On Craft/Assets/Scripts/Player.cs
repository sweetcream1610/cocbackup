﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Player : MonoBehaviour
{
    public GameObject player;
    [SerializeField] private GameObject menu;
    public int menuCode;
    public float speed = 0.1f;
   void Start()
    {
        
        menu.SetActive(false);
        //ゲーム開始時にカーソルを非表示にする
        Cursor.lockState = CursorLockMode.Locked;

    }
    void Update()
    {
        UserController();
    }
    /// <summary>
    ///  プレイヤーキャラクターの仕様
    /// </summary>
    public void UserController()
    {
        //以下移動操作
        if (Input.GetKey(KeyCode.W))
        {
            player.transform.Translate(Vector3.forward * speed);
        }
        if (Input.GetKey(KeyCode.S))
        {
            player.transform.Translate(Vector3.back * speed);
        }
        if (Input.GetKey(KeyCode.A))
        {
            player.transform.Translate(Vector3.left * speed);
        }
        if (Input.GetKey(KeyCode.D))
        {
            player.transform.Translate(Vector3.right * speed);
        }

        //メニュー画面の展開
        if (Input.GetKeyDown(KeyCode.Space)) {
            menuCode++;
            switch (menuCode)
            { 
                case 0:
                    menu.SetActive(false);
                    Debug.Log("コード指数:" + menuCode);
                    break;
                case 1:                     
                    menu.SetActive(true);                  
                    Cursor.lockState = CursorLockMode.None;
                    Debug.Log("コード指数：" + menuCode);
                    menuCode -= 2;                  
                    break;
                default:
                    menu.SetActive(true);
                    break;
            }           
        }
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("条件メニュー画面を開くゾーンに入っている");
    }
    private void OnTriggerExit(Collider other)
    {
        Debug.Log("条件メニューのゾーンを撤退");
    }

}
