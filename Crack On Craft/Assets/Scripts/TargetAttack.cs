﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// プレイヤー攻撃仕様
/// </summary>
public class TargetAttack : MonoBehaviour
{
    [SerializeField]public GameObject targets;
    [SerializeField]private GameObject playerAttack;
    [SerializeField]DataSheet ds;    
    [SerializeField] public int parAccessHP;
    [SerializeField] private UIFunction UI;
    void Start()
    {
        parAccessHP = ds.HP;
        UI.itemCounts = 0;
    }

    void Update()
    {
       
    }
    private void OnTriggerEnter(Collider other)
    {
        
        if (playerAttack)   //コライダーを呼び出した場合の処理
        {
            parAccessHP--;
            Debug.Log("HP残り　" + parAccessHP);
            Debug.Log("削除ログ " + targets);
            if (parAccessHP == 0) //敵を削除した時の処理
            {
                 Destroy(targets.transform.gameObject);
                UI.itemCounts++;

            }
        }
    }
}
//Destroy(targets.transform.gameObject);