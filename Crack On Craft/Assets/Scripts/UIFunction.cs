﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
 public class UIFunction : MonoBehaviour
{
    /// <summary>
/// アイテムインベントリのオブジェクト
/// </summary>
    [SerializeField]
    private GameObject itemBox;
   /// <summary>
   /// アイテムインベントリの切り替え値
   /// </summary>
    public int mCode;    //アイテムの表示を切り替える値
    [SerializeField]
    Player playerConnect;
    /// <summary>
    /// メニュー画面の切り替え値
    /// </summary>
    int mConnect;
    /// <summary>
    /// ゲーム画面にアクセスする関数
    /// </summary>
    Text itemText;
    /// <summary>
    /// DataSheetの呼び出し
    /// </summary>
    [SerializeField]DataSheet dsinUI;
    /// <summary>
    /// TargetAttack>>>>>>UIFunctionに呼び出し
    /// </summary>
    [SerializeField] TargetAttack tainUIF;
    /// <summary>
    /// アイテム情報
    /// </summary>
    [SerializeField]public int itemCounts;[SerializeField]int parHP;
    [SerializeField] Text texter;[SerializeField] public GameObject textar;
    private bool catcing = false;
    public GameObject target;

   
    private void Start()
    {
        target = tainUIF.targets;
       // itemCounts = dsinUI.ItemData;
        itemBox.SetActive(false);
              mConnect = playerConnect.menuCode; //プレイヤークラスからメニュー画面の表示切り替え値を獲得
        texter = textar.GetComponent<Text>();//ゲームオブジェクトにテキストをアタッチ。定義したテキストオブジェクトに記入する
        parHP = tainUIF.parAccessHP;
        Debug.Log("アイテムインベントリ更新\n対象のアイテム：" + dsinUI.ItemName);
    }
    private void Update()
    {
        if (parHP == 0)
        {
            catcing = true;
            //++itemCounts;
            if(!catcing)
            {
                Destroy(target.transform.gameObject);
            }
        }
        texter.text = dsinUI.ItemName + itemCounts;
    }

    public void Maiden()
    {       
        SceneManager.LoadScene("Game");
    }
    /// <summary>
    /// ゲームを終了する関数
    /// </summary>
    public void GameEnd() 
    {
        UnityEditor.EditorApplication.isPlaying = false;
        UnityEngine.Application.Quit();
    }
    public void ItemBox()
    {
        //後期修正項目：アイテムインベントリを開いたらメニューの開閉でインベントリのみ開くバグ有り。
        ++mCode;
        switch (mCode)
        {
            case 0:                
                itemBox.SetActive(false);
                break;
            case 1:
                itemBox.SetActive(true);
                mCode -= 2;
                break;
        }
            Debug.Log("コードが変わりました：" + mCode);
   
    }
    /// <summary>
    /// コメントアウトしたソースの収納
    /// </summary>

 
    private void TextBox()
    {   //ItemBox
        if (mCode >= 1)                                     //アイテム画面がコード1になったら、アイテムインベントリ展開
        {
            itemBox.SetActive(true);
            Debug.Log("アイテム画面の展開");
        }

        if (mCode != 1 & mConnect != 1)                     //メニューが開いてて、かつコード1以上なら、インベントリ閉鎖
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                mCode--;
                itemBox.SetActive(false);
                Debug.Log("アイテムコード：" + mCode);
            }
        }
        if (mCode >= 2)
        {
            mCode -= 2;
            Debug.Log("コード初期化");
            itemBox.SetActive(false);
        }

    }
}

/// <summary>
/// カウント関数。アイテムの代入参考
/// </summary>
//public void TimeCount()
//{
//    float timeCounts1 = 1f;
//    timeCounts0 += Time.deltaTime;
//    if (timeCounts0 > timeCounts1)
//    {
//        timeCounts0 = 0;
//        Debug.Log("いーち");

//    }
//}

