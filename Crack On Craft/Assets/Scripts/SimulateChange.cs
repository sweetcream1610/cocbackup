﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SimulateChange : MonoBehaviour
{
   [SerializeField] private GameObject townsWarp;
   [SerializeField]private GameObject gamesWarp;
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        //エリアのワープオブジェクトに触れたらステージと拠点間を行き来する
        if (townsWarp) {
            SceneManager.LoadScene("Game");
        }
        else if (gamesWarp)
        {
            SceneManager.LoadScene("TOWN");
        }
    }
    /// <summary>
    /// 拠点専用メニュー画面の仕様。menusigh変数でボタンをクリックした際の反応を設定。
    /// </summary>
  
}
