﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// カメラ仕様スクリプト
/// </summary>
public class UserCamera : MonoBehaviour
{
    [SerializeField] Camera cam;
    [SerializeField] Player player;
    [SerializeField] Vector3 lookAngle = new Vector3(0f, 0f, 0f);
    [SerializeField] float posAxis;
    [SerializeField] float rotAxis;

    // Start is called before the first frame update
    void Start()
    {
        lookAngle = transform.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        CamRotateControll();
    }
    /// <summary>
    /// カメラ操作
    /// </summary>
    private void CamRotateControll()
    {
        //カメラの回転具合の定義
        float rotateSpeed = 2.0f;
        lookAngle = new Vector3(Input.GetAxis("Mouse X") * rotateSpeed, Input.GetAxis("Mouse Y") * rotateSpeed, 0);

        //RotateAround()でメインカメラを回転
        cam.transform.RotateAround(player.transform.position, Vector3.up, lookAngle.x);
    //    cam.transform.RotateAround(player.transform.position, transform.right, lookAngle.y);

    }

}