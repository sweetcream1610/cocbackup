﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gimmick : MonoBehaviour
{
   // Player player_gimmick;
    [SerializeField]float player_speed;
    [SerializeField] Player gimmick_player;
    [SerializeField] GameObject menu_point;
    private int menusigh;                               //拠点専用メニューの制御変数
    [SerializeField] private GameObject pointMenu_Close;//拠点専用メニューの終了ボタン
    private Player prodact;
    void Start()
    {
     //   player_speed = gimmick_player.speed;
    }

    // Update is called once per frame
    void Update()
    {
        PointMenuSelect();        
         
        }
    private void PointMenuSelect()
    {

        switch (menusigh)
        {
            case 1:
                pointMenu_Close.SetActive(false);
                menusigh = 0;
                break;
            case 2:
                SceneManager.LoadScene("StageCreate");
                break;
            case 3:
                SceneManager.LoadScene("StageSelect");
                break;
            case 4:
                SceneManager.LoadScene("ItemList");
                break;
        }
    }
    //menusigh変数の値の変更をこの各関数で変える。
    public void Menusigh_StageCreate()
    {
        menusigh = 2;
    }
    public void Menusigh_StageSelect()
    {
        menusigh = 3;
    }
    public void Menusigh_ItemList()
    {
        menusigh = 4;
    }
    public void Menusigh_CloseMenu()
    {
        menusigh = 1;
    }
    private void OnTriggerEnter(Collider other)
    {
        player_speed = 0f;
        menu_point.SetActive(true);
        Cursor.lockState = CursorLockMode.None;
        player_speed = 0;
    }
    private void OnTriggerExit(Collider other)
    {
        player_speed = 0.1f;
        menu_point.SetActive(false);
        Cursor.lockState = CursorLockMode.Locked;
     }
}
